import requests

def scanner(domain_name, file_domain_list="subdomain.txt"):
    with open(file_domain_list, "r") as file:
        file = file.read()
        sub_domain_list = file.splitlines()

    print(f"----- Scanning for : {domain_name} ----")
    for sub_domain in sub_domain_list:
        url = f"https://{sub_domain}.{domain_name}"
        try:
            requests.get(url)
            print(f"[+] {url}")
        except requests.ConnectionError:
            pass

if __name__ == "__main__":
    domainName = input("\n[!] Write the domain name here : ")

    defaultList = ""

    while defaultList != "Y" and defaultList != "N":
        defaultList = input("[!] Do you want to use the default domain list ? Y/N : ")
        defaultList = defaultList.upper()

    if defaultList == "Y":
        scanner(domainName)
    else:
        domains_list = input("[!] Write the file of domains : \n")
        scanner(domainName, domains_list)